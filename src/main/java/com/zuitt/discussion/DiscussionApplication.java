package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//Will require all routes within the "Discussion Application" to use "/greeting" as part of its routes
@RequestMapping("/greeting")
//The @RestController tells Spring boot that this application will function as an endpoint that will be used in handling web requests
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	//Maps a get request to the route "/hello" and the method "hello()"
	public String hello() {
		return "Hello World!";
	}

	//Routes with a string query
	//Dynamic data is obtained from the URL's string query
	//"%s" specifies

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	//Multiple Parameters
	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	//Routes with path variables
	@GetMapping("/hello/{name}")
	//localhost:8080/hello/joe
	//@PathVariable annotation allows us to extract data directly from the URL
	public String courses(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}


	//-----------------------
	//ACTIVITY - S09-activity
	//-----------------------

	ArrayList enrollees = new ArrayList<>();

	@GetMapping("/enroll")
	//http://localhost:8080/greeting/enroll?user=Benhur
	public String enroll(@RequestParam String user) {
		enrollees.add(user);
		return "Thank you for enrolling, " + user + "!";
	}

	@GetMapping("/getEnrollees")
	//http://localhost:8080/greeting/getEnrollees
	public ArrayList getEnrollees() {
		return enrollees;
	}

	@GetMapping("/nameage")
	//http://localhost:8080/greeting/nameage?name=Benhur&age=21
	public String nameAge(@RequestParam String name, @RequestParam int age) {
		return "Hello " + name + "! My age is " + age + ".";
	}

	@GetMapping("/courses/{id}")
	//http://localhost:8080/greeting/courses/java101
	public String courseId(@PathVariable ("id") String id) {
		String message;
		if(id.equals("java101")){
			message = "Name: Java 101, Schedule: MWF 8:00 AM - 11:00 AM, Price: PHP 3000";
		}
		else if(id.equals("sql101")){
			message = "Name: SQL 101, Schedule: TTH 3:30 PM - 5:30 PM, Price: PHP 3200";
		}
		else if(id.equals("javaee101")){
			message = "Name: Java EE101, Schedule: MWF 12:00 PM - 1:30 PM, Price: PHP 3500";
		}
		else{
			message = "The course cannot be found!";
		}

		return message;
	}
}
